package com.zk.com;

import java.io.IOException;
import java.util.List;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.junit.Before;
import org.junit.Test;

/**
* @Description: 
* @author wangaq
* @date 2018年10月19日
*
*/

public class ZookeeperTest {
	
	private static String connectStr = "hadoop001:2181,hadoop002:2181,hadoop003:2181";
	private static int sessionTimeout = 2000;
	
	private ZooKeeper zk = null;
	
	@Before
	public void init() {
		try {
			zk = new ZooKeeper(connectStr, sessionTimeout, new Watcher() {
				
				public void process(WatchedEvent event) {
					// 收到事件通知后的回调函数（用户的业务逻辑）
					EventType type = event.getType();
					String path = event.getPath();
					System.out.println(type + "--" + path);
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void create() {
		// 创建子节点
		// 参数1：要创建的节点路径  参数2：节点数据 参数3：节点权限 参数4：节点类型
		try {
			String create = zk.create("/eclipse/app", "hello app".getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
			System.out.println(create);
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void getChildren() {
		// 获取子节点
		try {
			List<String> children = zk.getChildren("/eclipse/app", Boolean.TRUE);
			for (String str : children) {
				System.err.println(str);
			}
			
			Thread.sleep(Long.MAX_VALUE);
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void exist() {
		// 判断节点是否存在
		try {
			Stat exists = zk.exists("/eclipse", Boolean.FALSE);
			System.out.println(exists.getVersion());
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void delete() {
		try {
			Stat exists = zk.exists("/eclipse/app", Boolean.FALSE);
			System.out.println(exists.getVersion());
			
			zk.delete("/eclipse/app", exists.getVersion());
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
